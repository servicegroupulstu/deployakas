# Backend
git clone https://gitlab.com/servicegroupulstu/javaserver.git

# Media service
git clone https://gitlab.com/servicegroupulstu/mediaservice.git

# Frontend
git clone https://gitlab.com/servicegroupulstu/akas.git

# Start project
docker-compose up -d
